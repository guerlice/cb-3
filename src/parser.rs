use crate::C1Token::{
    And, Assign, Asterisk, ConstBoolean, ConstFloat, ConstInt, Equal, Greater, GreaterEqual,
    Identifier, KwBoolean, KwFloat, KwIf, KwInt, KwPrintf, KwReturn, KwVoid, LeftBrace,
    LeftParenthesis, Less, LessEqual, Minus, NotEqual, Or, Plus, RightBrace, RightParenthesis,
    Semicolon, Slash,
};
use crate::{C1Lexer, C1Token, ParseResult};

pub struct C1Parser<'a> {
    lexer: C1Lexer<'a>,
}

const EXPR: [C1Token; 6] = [Equal, NotEqual, LessEqual, GreaterEqual, Less, Greater];
const SIMPEXPR: [C1Token; 3] = [Plus, Minus, Or];
const TERM: [C1Token; 3] = [Asterisk, Slash, And];

impl C1Parser<'_> {
    pub fn parse(text: &str) -> ParseResult {
        C1Parser {
            lexer: C1Lexer::new(text),
        }
        .begin_program()
    }
    fn begin_program(&mut self) -> ParseResult {
        while let Some(_) = self.lexer.current_token() {
            if let Err(_) = self.function_decoding() {
                break;
            }
        }
        Ok(())
    }
    fn function_decoding(&mut self) -> ParseResult {
        self.type_verification()?;
        self.consume_and_confirm(Some(Identifier))?;
        self.consume_and_confirm(Some(LeftParenthesis))?;
        self.consume_and_confirm(Some(RightParenthesis))?;
        self.consume_and_confirm(Some(LeftBrace))?;
        self.statement_collection()?;
        self.consume_and_confirm(Some(RightBrace))
    }
    fn type_verification(&mut self) -> ParseResult {
        match self.lexer.current_token() {
            Some(KwBoolean | KwFloat | KwInt | KwVoid) => {
                self.lexer.eat();
                Ok(())
            }
            _ => Err(format!(
                "Error in line: {} expected type got {:?}",
                self.lexer.current_line_number().unwrap_or(0),
                self.lexer.current_token().unwrap_or(C1Token::Identifier)
            )),
        }
    }
    fn statement_collection(&mut self) -> ParseResult {
        while let Some(_) = self.lexer.current_token() {
            if let Err(_) = self.process_block() {
                break;
            }
        }
        Ok(())
    }
    fn consume_and_confirm(&mut self, token: Option<C1Token>) -> ParseResult {
        if self.lexer.current_token() == token {
            self.lexer.eat();
            return Ok(());
        }
        return Err(format!(
            "Error in line: {} got {:?}",
            self.lexer.current_line_number().unwrap_or(0),
            self.lexer.current_token().unwrap_or(C1Token::Identifier)
        ));
    }
    fn process_block(&mut self) -> ParseResult {
        if self.lexer.current_token() == Some(LeftBrace) {
            self.lexer.eat();
            self.statement_collection()?;
            self.consume_and_confirm(Some(RightBrace))
        } else {
            self.process_statement()
        }
    }
    fn process_statement(&mut self) -> ParseResult {
        match self.lexer.current_token() {
            Some(KwIf) => self.ifstatement_processing(),
            Some(KwReturn) => {
                self.returnstatement_processing()?;
                self.consume_and_confirm(Some(Semicolon))
            }
            Some(KwPrintf) => {
                self.printstatement_processing()?;
                self.consume_and_confirm(Some(Semicolon))
            }
            Some(Identifier) => {
                if self.lexer.peek_token() == Some(Assign) {
                    self.statement_assignment()?;
                    return self.consume_and_confirm(Some(Semicolon));
                } else if self.lexer.peek_token() == Some(LeftParenthesis) {
                    self.call_function()?;
                    return self.consume_and_confirm(Some(Semicolon));
                }
                Err(format!(
                    "Error in line: {} expected statement got {:?}",
                    self.lexer.current_line_number().unwrap_or(0),
                    self.lexer.current_token().unwrap_or(C1Token::Identifier)
                ))
            }
            _ => Err(format!(
                "Error in line: {} expected statement got {:?}",
                self.lexer.current_line_number().unwrap_or(0),
                self.lexer.current_token().unwrap_or(C1Token::Identifier)
            )),
        }
    }
    fn ifstatement_processing(&mut self) -> ParseResult {
        self.consume_and_confirm(Some(KwIf))?;
        self.consume_and_confirm(Some(LeftParenthesis))?;
        self.assignment_processing()?;
        self.consume_and_confirm(Some(RightParenthesis))?;
        self.process_block()
    }
    fn returnstatement_processing(&mut self) -> ParseResult {
        self.consume_and_confirm(Some(KwReturn))?;
        if self.lexer.current_token() != Some(Semicolon) {
            self.assignment_processing()?;
        }
        Ok(())
    }
    fn printstatement_processing(&mut self) -> ParseResult {
        self.consume_and_confirm(Some(KwPrintf))?;
        self.consume_and_confirm(Some(LeftParenthesis))?;
        self.assignment_processing()?;
        self.consume_and_confirm(Some(RightParenthesis))
    }
    fn statement_assignment(&mut self) -> ParseResult {
        self.consume_and_confirm(Some(Identifier))?;
        self.consume_and_confirm(Some(Assign))?;
        self.assignment_processing()
    }
    fn call_function(&mut self) -> ParseResult {
        self.consume_and_confirm(Some(Identifier))?;
        self.consume_and_confirm(Some(LeftParenthesis))?;
        self.consume_and_confirm(Some(RightParenthesis))
    }
    fn assignment_processing(&mut self) -> ParseResult {
        if self.lexer.peek_token() == Some(Assign) {
            self.lexer.eat();
            self.consume_and_confirm(Some(Assign))?;
            self.assignment_processing()
        } else {
            self.expression_processing()
        }
    }
    fn expression_processing(&mut self) -> ParseResult {
        self.simple_expression_processing()?;
        while EXPR.contains(&self.lexer.current_token().unwrap_or(C1Token::Identifier)) {
            self.lexer.eat();
            self.simple_expression_processing()?;
        }
        Ok(())
    }
    fn simple_expression_processing(&mut self) -> ParseResult {
        if self.lexer.current_token() == Some(Minus) {
            self.lexer.eat();
        }
        self.term_processing()?;
        while SIMPEXPR.contains(&self.lexer.current_token().unwrap_or(C1Token::Identifier)) {
            self.lexer.eat();
            self.term_processing()?;
        }
        Ok(())
    }
    fn term_processing(&mut self) -> ParseResult {
        self.factor_processing()?;
        while TERM.contains(&self.lexer.current_token().unwrap_or(C1Token::Identifier)) {
            self.lexer.eat();
            self.factor_processing()?;
        }
        Ok(())
    }
    fn factor_processing(&mut self) -> ParseResult {
        match self.lexer.current_token() {
            Some(ConstInt) | Some(ConstFloat) | Some(ConstBoolean) => {
                self.lexer.eat();
                Ok(())
            }
            Some(Identifier) => {
                if self.lexer.peek_token() == Some(LeftParenthesis) {
                    return self.call_function();
                }
                self.lexer.eat();
                Ok(())
            }
            Some(LeftParenthesis) => {
                self.lexer.eat();
                self.assignment_processing()?;
                self.consume_and_confirm(Some(RightParenthesis))
            }
            _ => Err(format!(
                "Error in line: {} expecting factor got {:?}",
                self.lexer.current_line_number().unwrap_or(0),
                self.lexer.current_token().unwrap_or(C1Token::Identifier)
            )),
        }
    }
}
